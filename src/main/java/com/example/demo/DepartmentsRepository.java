package com.example.demo;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface DepartmentsRepository extends CrudRepository<Departments, Long>  {
	 public List<Departments> findById(long id);
}