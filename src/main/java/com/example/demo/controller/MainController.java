package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Departments;
import com.example.demo.DepartmentsRepository;

@RestController
public class MainController {
  @Autowired 
  private DepartmentsRepository departmentsRepository;
  
 @PostMapping(path="/add")
  public @ResponseBody Iterable addNewDepartment (@RequestParam Long departmentId, 
		  @RequestParam String departmentName, @RequestParam Long managerId, 
		  @RequestParam Long locationId) {
    Departments newDepartment = new Departments();
    newDepartment.setDepartmentId(departmentId);
    newDepartment.setDepartmentName(departmentName);
    newDepartment.setManagerId(managerId);
    newDepartment.setLocationId(locationId);
    departmentsRepository.save(newDepartment);
    Iterable<Departments> updatedList = departmentsRepository.findAll();
    return updatedList;
  }
	
  @GetMapping(path="/list")
  public @ResponseBody Iterable<Departments> getList(Model model) {
      Iterable<Departments> listDep = departmentsRepository.findAll();
      return listDep;
  }
  
  @RequestMapping(value = "/anonymous", method = RequestMethod.GET)
  public ResponseEntity<String> getAnonymous() {
      return ResponseEntity.ok("Hello Anonymous");
  }

  @RequestMapping(value = "/user", method = RequestMethod.GET)
  public ResponseEntity<String> getUser() {
      return ResponseEntity.ok("Hello User");
  }

  @RequestMapping(value = "/admin", method = RequestMethod.GET)
  public ResponseEntity<String> getAdmin() {
      return ResponseEntity.ok("Hello Admin");
  }

  @RequestMapping(value = "/all-user", method = RequestMethod.GET)
  public ResponseEntity<String> getAllUser() {
      return ResponseEntity.ok("Hello All User");
  }
  
}