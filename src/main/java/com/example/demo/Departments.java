package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "HR", name = "DEPARTMENTS")
public class Departments {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long department_id;
	private String department_name;
	private Long manager_id;
	private Long location_id;
	
	public Long getDepartmentId() {
		return department_id;
	}
	
	public void setDepartmentId(Long id) {
		department_id = id;
	}
	
	public String getDepartmentName() {
		return department_name;
	}
	
	public void setDepartmentName(String name) {
		this.department_name = name;
	}
	
	public Long getManagerId() {
		return this.manager_id;
	}
	
	public void setManagerId(Long id) {
		this.manager_id = id;
	}
	
	public Long getLocationId() {
		return this.location_id;
	}
	
	public void setLocationId(Long id) {
		this.location_id = id;
	}
}
